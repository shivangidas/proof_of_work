import boto3
import botocore
import time
from list_instances import getNotDeadHosts
from delete_security_group import delete_security
from key_delete import delete_key
ec2 = boto3.resource('ec2')

instances = ec2.instances.filter(
    Filters=[{'Name': 'instance-state-name', 'Values': ['stopped', 'running', 'pending']}])
# Collect instance ids
ids = []
for instance in instances:
    print(instance.id)
    ids.append(instance.id)

# Terminate all instances
ec2.instances.filter(InstanceIds=ids).terminate()

while len(getNotDeadHosts()) != 0:
    time.sleep(5)
    print("Waiting for instances to die...")
    continue

# delete security group
delete_security()
# delete key pair
delete_key()
