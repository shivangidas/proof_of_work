# Proof_Of_Work

Cloud Computing Coursework

Horizontal Scaling for an Embarrassingly Parallel Task: Blockchain Proof-of-Work in the Cloud

## AWS CLI

1. Get AWS CLI
   <pre>pip3 install awscli --upgrade --user</pre>
2. Add to path
   <pre>export PATH=~/.local/bin:$PATH</pre>
   Check with
    <pre>aws --version</pre>
   output something like: aws-cli/1.16.283 Python/3.7.4 Darwin/18.7.0 botocore/1.13.19
3. AWS configure

   Open and edit credentials from Account Details

      <pre>open ~/.aws/credentials</pre>

   Add region too, region=us-east-1

## Install boto framework

- <pre>pip install boto3</pre>

## Get fabric

- <pre>pip install 'fabric<2.0'</pre>

## Set up on AWS

    Here,
    rabbitmqHost = ec2 instance 1
    workerHosts = ec2 instance 2, 3, ...
    allHosts = all ec2 instances created

1. Create a keypair nonce and add to this folder. This step is to be done only once.
   <pre>python key_pair.py</pre>
2. Create instance and security group.
   Mention the number of instances. Default is 2

     <pre>python create_ec2.py 4</pre>

3. After creating instances, wait a couple of seconds (check with **fab allHosts** to see if the instances are running) and run the following to install Docker and transfer files.

   <pre>fab allHosts getDocker transferFiles</pre>

   **This will require you to type in 'Y' for Docker installation on all instances.** (Sorry I couldn't automate it.)

   Also things may timeout, suggest doing this for hosts 3-4 at a time.

   Alternatively, get the hosts with **fab allHosts** and run **fab getDocker transferFiles**. Paste in the host when asked.

   Or just run the command again.

4. Create an overlay network

   <pre>fab rabbitmqHost createNetwork</pre>

5. Get swarm token and add to other ec2 instances

   <pre>fab rabbitmqHost getToken</pre>
   <pre>fab workerHosts connectToSwarm:"command-given-by-getToken"</pre>

   Example:

   <pre>fab workerHosts connectToSwarm:"docker swarm join --token SWMTKN-1-2u30fjiwhl6z8fb2oclmpb12l152fidj3ecwr93whq34fuizns-f26tdpfu6fzecq0fdt9772les 172.31.82.248:2377"</pre>

- See/check the network and nodes

    <pre>fab rabbitmqHost checkNetwork</pre>
    <pre>fab rabbitmqHost node</pre>
    <pre>fab rabbitmqHost inspectNetwork</pre>

  Instead of rabbitmqHost, allHosts and workerHosts or specifying host ip also works

6. Deploy stack

   <pre>fab rabbitmqHost deployStack</pre>

7. Scale stack to number of ec2 instances minus 1
   <pre>fab rabbitmqHost scaleWorker:3</pre>

- Check services in the stack

    <pre>fab rabbitmqHost checkStack</pre>

  If all replicas are running you'll have something like 1/1 and 3/3

8.  Get one worker container id for running code

    <pre>fab allHosts dockerps</pre>
    <pre>fab allHosts runCode:containerid</pre>

    Example

    <pre>fab allHosts runCode:69ca69a2ce06</pre>

    Alternatively skip the **allHosts** and give the ec2 ip when asked <pre>No hosts found. Please specify (single) host string for connection: </pre>

9.  When root@69ca69a2ce06:/app# shows up, type in the following

    <pre>python -m container_app.submit_jobs 5 100 30</pre>

    where,
    **5- difficulty**,
    **100- time in seconds**,
    **30- number of tasks at a time**

    Type in **exit** when done

10. To increase the number of instances and workers, repeat step 2, 3, 5 and 7 for the new instances

## Logs for services

<pre>fab rabbitmqHost rabbitmqLog</pre>
<pre>fab oneHost workerLog</pre>

## Take things down

### Directly terminate all instances

<pre>python terminate_all_ec2.py</pre>

This stops the instances, waits for them to terminate, removes security group and key pair

### Only remove the containers or services

Remove stack <pre>fab rabbitmqHost stackRemove</pre>
OR kill workers <pre>fab allHosts killWorkers</pre>
Remove workers <pre>fab allHosts removeWorkers</pre>

## Leave swarm

<pre>fab allHosts leaveSwarm</pre>

### Manual ssh

Can shh using

<pre>ssh -i nonce.pem ubuntu@ec2-54-161-108-161.compute-1.amazonaws.com</pre>

or <pre>ssh -i nonce.pem ubuntu@54.234.5.139</pre>

### Some python code for managing ec2

- Create instances and security group
    <pre>python create_ec2.py</pre>
- Create key pair
    <pre>python key_pair.py</pre>
- Stop instance with instance id
    <pre>python start_stop_ec2.py off i-09cc5214582b7d358</pre>
- Stop all instances
    <pre>python stop_all_ec2.py</pre>
- Terminate all instances
  <pre>python terminate_all_ec2.py</pre>
- Delete Security Group
    <pre>python delete_security_group.py</pre>
- List running hosts
  <pre>python list_instances.py</pre>
