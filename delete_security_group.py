import boto3
from botocore.exceptions import ClientError
import sys

ec2 = boto3.client('ec2')


def delete_security():
    try:
        response = ec2.delete_security_group(
            GroupName='nonceSecurity')
        print('Security Group Deleted')
    except ClientError as e:
        print(e)


if __name__ == '__main__':
    delete_security()
