
from fabric.api import *
from fabric.state import env
from list_instances import getHosts
import os

env.key_filename = "nonce.pem"
env.user = "ubuntu"


@task
def allHosts():
    hosts = getHosts()
    env.hosts = hosts
    print('Number of running instances: ', len(hosts))


@task
def rabbitmqHost():
    hosts = getHosts()
    env.hosts = hosts[0]
    print("working on: ", hosts[0])


@task
def workerHosts():
    hosts = getHosts()
    env.hosts = hosts[1:]


@task
def oneHost():
    hosts = getHosts()
    env.hosts = hosts[1]

# test
@task
def whoami():
    run('whoami')
    sudo('whoami')


@task
def getDocker():
    # run('snap install docker')
    sudo('apt update')
    sudo('apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common')
    sudo('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -')
    sudo('add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable "')
    sudo('apt-get install docker-ce docker-ce-cli -y')


# @task
# def tryDocker():
#     sudo("yum update - y")
#     sudo("yum install - y docker")


@task
def dockerwhich():
    run('which docker')


@task
def getRabbitmq():
    sudo('docker pull rabbitmq:latest')


@task
def createNetwork():
    hosts = getHosts()
    env.hosts = hosts[0]
    sudo('docker swarm init')
    sudo('docker network ls')
    sudo('docker network create -d overlay --attachable mynetwork')
    sudo('docker network ls')


@task
def checkNetwork():
    sudo('docker network ls')


@task
def getToken():
    sudo('docker swarm join-token manager')


@task
def connectToSwarm(token):
    sudo(token)


@task
def leaveSwarm():
    sudo('docker swarm leave --force')


@task
def scaleWorker(num):
    command = 'docker service scale mynetwork_worker=' + num
    sudo(command)


@task
def startRabbitmq():
    command = 'docker run --rm --name rabbit -p 5672:5672 --env RABBITMQ_DEFAULT_USER=admin --env RABBITMQ_DEFAULT_PASS=mypass --network mynetwork rabbitmq:latest'
    sudo(command)


@task
def transferFiles():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    path = dir_path + '/dockerapp'
    print(path)
    put(path, '~/')


@task
def runDockercompose():
    with cd('/home/ubuntu/dockerapp'):
        sudo('docker-compose build')
        command = 'docker-compose up'
        sudo(command)


@task
def deployStack():
    with cd('/home/ubuntu/dockerapp'):
        sudo('docker stack deploy -c docker-compose.yml mynetwork')


@task
def checkStack():
    sudo('docker stack services mynetwork')


@task
def node():
    sudo('docker node ls')


@task
def inspectNetwork():
    sudo('docker network inspect mynetwork')


@task
def rabbitmqLog():
    sudo('docker service logs mynetwork_rabbit')


@task
def workerLog():
    sudo('docker service logs mynetwork_worker')


@task
def buildWorker():
    with cd('/home/ubuntu/dockerapp'):
        command = 'docker build . -t worker'
        sudo(command)


@task
def startWorker():
    with cd('/home/ubuntu/dockerapp'):
        command = 'docker run --network mynetwork --link rabbit -v $(pwd):/app worker'
        sudo(command)


@task
def runCode(containerid):
    try:
        with cd('/home/ubuntu/dockerapp'):
            command = 'docker exec -it ' + containerid + ' /bin/bash'
            sudo(command)
    except:
        print("Container not here. Checking next instance....")


@task
def dockerps():
    sudo('docker ps')


@task
def down():
    with cd('/home/ubuntu/dockerapp'):
        sudo('docker-compose down')


@task
def stackRemove():
    sudo('docker stack rm mynetwork')


@task
def killWorkers():
    try:
        containers = sudo('docker ps -q')
        if len(containers) > 0:
            command = 'docker kill $(docker ps -q)'
            sudo(command)
    except Exception as e:
        print(e)


@task
def removeWorkers():
    try:
        containers = sudo('docker ps -a -q')
        if len(containers) > 0:
            command = 'docker rm $(docker ps -a -q)'
            sudo(command)
    except Exception as e:
        print(e)


@task
def rabbitMQCredentials(user, password):
    command = 'export RABBITMQ_DEFAULT_USER='+user
    sudo(command)
    command = 'export RABBITMQ_DEFAULT_PASS=' + password
    sudo(command)
