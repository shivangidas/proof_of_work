from botocore.exceptions import ClientError
import boto3
import sys

ec2_resource = boto3.resource('ec2')

ec2 = boto3.client('ec2')


def createEC2(workers=2):
    # createKey()
    security_group_id = ""

    group_name = 'nonceSecurity'
    try:
        response = ec2.describe_security_groups(
            Filters=[
                dict(Name='group-name', Values=[group_name])
            ]
        )
        security_group_id = response['SecurityGroups'][0]['GroupId']
    except:
        print("Creating new security group......\n")
    if security_group_id:
        print(security_group_id)
    else:
        try:
            response = ec2.create_security_group(GroupName=group_name,
                                                 Description='custom group')
            security_group_id = response['GroupId']
            print('Security Group Created %s ' % (security_group_id))
            data = ec2.authorize_security_group_ingress(
                GroupId=security_group_id,
                IpPermissions=[
                    {'IpProtocol': 'tcp',
                     'FromPort': 80,
                     'ToPort': 80,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 22,
                     'ToPort': 22,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 5672,
                     'ToPort': 5673,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 4369,
                     'ToPort': 4369,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 56721,
                     'ToPort': 56721,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 2376,
                     'ToPort': 2377,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 25672,
                     'ToPort': 25672,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'tcp',
                     'FromPort': 7946,
                     'ToPort': 7946,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'udp',
                     'FromPort': 7946,
                     'ToPort': 7946,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
                    {'IpProtocol': 'udp',
                     'FromPort': 4789,
                     'ToPort': 4789,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
                ])
            print('Ingress Successfully Set %s' % data)
        except ClientError as e:
            print(e)
    for i in range(0, workers):
        try:
            # create a new EC2 instance
            instances = ec2_resource.create_instances(
                ImageId='ami-04b9e92b5572fa0d1',
                MinCount=1,
                MaxCount=1,
                InstanceType='t2.micro',
                KeyName='nonce',
                SecurityGroupIds=[security_group_id]
            )
            print(instances)
        except ClientError as e:
            print(e)


if __name__ == '__main__':
    workers = 2
    if len(sys.argv) > 1:
        if (int(sys.argv[1]) > 10):
            print("{} EC2 instances are too much for me to handle. Exiting...\n".format(
                sys.argv[1]))
            exit(0)
        workers = int(sys.argv[1])
    createEC2(workers)
