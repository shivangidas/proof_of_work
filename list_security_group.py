import boto3
from botocore.exceptions import ClientError
import sys


def list():
    ec2 = boto3.client('ec2')
    group_name = 'nonceSecurity'
    response = ec2.describe_security_groups(
        Filters=[
            dict(Name='group-name', Values=[group_name])
        ]
    )
    group_id = response['SecurityGroups'][0]['GroupId']
    print(group_id)


if __name__ == '__main__':
    list()
