import boto3

client = boto3.client('ec2')


def getHosts():
    instance_dict = client.describe_instances().get('Reservations')

    hosts = []

    for reservation in instance_dict:
        for instance in reservation['Instances']:
            if instance[u'State'][u'Name'] == 'running' and instance.get(u'PublicIpAddress') is not None:
                hosts.append(instance.get(u'PublicIpAddress'))
            elif instance[u'State'][u'Name'] == 'running' and instance.get(u'PublicDnsName') is not None:
                hosts.append(instance.get(u'PublicDnsName'))
    print(hosts)
    return hosts


def getNotDeadHosts():
    instance_dict = client.describe_instances().get('Reservations')

    hosts = []

    for reservation in instance_dict:
        for instance in reservation['Instances']:
            if instance[u'State'][u'Name'] != 'terminated' and instance.get(u'PublicIpAddress') is not None:
                hosts.append(instance.get(u'PublicIpAddress'))
            elif instance[u'State'][u'Name'] != 'terminated' and instance.get(u'PublicDnsName') is not None:
                hosts.append(instance.get(u'PublicDnsName'))
    return hosts


if __name__ == '__main__':
    getHosts()
