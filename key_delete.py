import boto3

ec2 = boto3.client('ec2')


def delete_key():
    response = ec2.delete_key_pair(KeyName='nonce')
    print("Key Pair Deleted")


if __name__ == '__main__':
    delete_key()
