import boto3
import os
from botocore.exceptions import ClientError
from key_delete import delete_key
ec2 = boto3.client('ec2')


def createKey():
    try:
        response = ec2.describe_key_pairs(KeyNames=['nonce'])
        # print(response)
        delete_key()  # delete if key pair exists
    except ClientError as e:
        print(e)  # does not exist. Proceed...
        print("Creating new key-pair")
    key = ec2.create_key_pair(KeyName='nonce')
    if os.path.exists('nonce.pem'):
        os.chmod('./nonce.pem', 0o777)
    with open('./nonce.pem', 'w') as file:
        file.write(key['KeyMaterial'])
    os.chmod('./nonce.pem', 0o444)


if __name__ == '__main__':
    createKey()
