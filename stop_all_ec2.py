import boto3
import botocore

# Retrieve the list of existing instances
ec2 = boto3.resource('ec2')

instances = ec2.instances.filter(
    Filters=[{'Name': 'instance-state-name', 'Values': ['stopped', 'running', 'pending']}])
# Collect instance ids
ids = []
for instance in instances:
    print(instance.id)
    ids.append(instance.id)
# Stop all instances
ec2.instances.filter(InstanceIds=ids).stop()
